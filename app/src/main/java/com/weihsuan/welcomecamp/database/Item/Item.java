package com.weihsuan.welcomecamp.database.Item;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.weihsuan.welcomecamp.database.SQLiteHelper;
import com.weihsuan.welcomecamp.Util.ToastUtil;
import com.weihsuan.welcomecamp.fragments.UpgradeFragment;

public class Item {
    public static final String NAME[]=
            {
                    "戰甲製造書"    ,"槍製造書" ,"盾製造書" ,"手套製造書","鞋製造書",
                    "布甲"     ,"木盾"     ,"木槍"    ,"工地手套"  ,"布鞋",
                    "星塵戰甲"  ,"星隕護盾" ,"流星之槍" ,"星塵護手"  ,"星石靈鞋",
                    "秘術鎧甲"  ,"神秘護盾" ,"秘術之槍" ,"祕法魔手"  ,"祕法靈靴",
                    "龍形鎧甲"  ,"龍形之盾" ,"幻龍長槍" ,"幻龍翼手"  ,"飛龍戰鞋",
                    "勇者的意志","全武器製造書"
            };
    public static final String deleteString[] =
            {
                    " 2 鐵"," 4 鐵"," 6 鐵"," 1 金 1 鐵"," 2 金"
            };

    private static final String KEY = "_id";
    public static final String TB = "item";
    private static final String COLUMN = "num";
    public static final String CREATE = "CREATE TABLE IF NOT EXISTS " +TB + " ( "+
            KEY + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN + " INTEGER)";
    public static int num[];
    public static int pro[] = {90,70,50,30};
    public static int fail[] = {0,0,0,30};
    private static int score[] = {1000,4000,10000,22000,45000,250000};
    private static SQLiteDatabase database;
    private static int count = 27;

    public static void init(Context mContext){
        database = SQLiteHelper.getDatabase(mContext);
        num = new int[count];
        getAll();
    }

    private static void initDatabase(){
        for (int i = 0; i < count; i++) {
            ContentValues cv = new ContentValues();
            cv.put(COLUMN,0);
            database.insert(TB,null,cv);
        }
    }

    public static void getAll(){
        Cursor c = database.rawQuery("SELECT * FROM "+ TB,null);
        if(c.moveToFirst()){
            for (int i = 0; i < count; i++) {
                num[i] = c.getInt(1);
                c.moveToNext();
            }
        }else {
            initDatabase();
        }
    }

    private static void update(){
        for (int i = 0; i < count; i++) {
            ContentValues cv = new ContentValues();
            cv.put(KEY,i+1);
            cv.put(COLUMN,num[i]);
            database.update(TB,cv,KEY+"="+(i+1),null);
        }
    }

    public static void add(int tar){
        num[tar]++;
        checkSut();
    }
    public static void upgrade(int tar){
        if(num[tar]>0 && tar<=19){
            int t = (int)(Math.random()*100);
            if(t<pro[tar/5]){
                num[tar]--;
                num[tar+5]++;
                checkSut();
                ToastUtil.show("升級成功");
            }
            else if(t>(100-fail[tar/5])) {
                num[tar]--;
                num[tar-5]++;
                ToastUtil.show("降級");
            }
            else {
                ToastUtil.show("升級失敗");
            }
            update();
        }
        else {
            if(tar==26){
                num[tar]--;
                num[UpgradeFragment.tar]++;
                update();
                return;
            }
            ToastUtil.show("錯誤");
        }
    }
    public static void delete(int tar){
        if(num[tar]>0){
            num[tar]--;
            ToastUtil.show("分解成功");
        } else {
            ToastUtil.show("數量不足");
        }
        checkSut();
    }
    private static void checkSut(){
        boolean sut = true;
        while(sut) {
            for (int i = 20; i < 25; i++) {
                if (num[i] == 0) {
                    sut = false;
                    break;
                }
            }
            if (sut) {
                for (int i = 20; i < 25; i++) {
                    num[i]--;
                }
                num[25]++;
            }
        }
        update();
    }
    public static int getScore(){
        getAll();
        int ret = 0;
        for (int i = 0; i < count; i++) {
            if(i==26){
                ret+=num[i]*score[0];
                continue;
            }
            ret += num[i]*score[i/5] ;
        }
        return ret;
    }
}
