package com.weihsuan.welcomecamp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.nfc.tech.NfcA;

import com.weihsuan.welcomecamp.database.Item.Item;

public class SQLiteHelper extends SQLiteOpenHelper {

    private static SQLiteDatabase database;
    private static final String NAME = "camp_db";
    private static final int VERSION = 2;

    public SQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public static SQLiteDatabase getDatabase(Context mContext){
        if(database==null||!database.isOpen()){
            database = new SQLiteHelper(mContext, NAME, null,VERSION).getWritableDatabase();
        }
        return database;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(Item.CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+ Item.TB );
        onCreate(sqLiteDatabase);
    }
}
