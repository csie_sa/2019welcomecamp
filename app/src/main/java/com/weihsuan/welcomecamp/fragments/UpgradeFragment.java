package com.weihsuan.welcomecamp.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.weihsuan.welcomecamp.R;
import com.weihsuan.welcomecamp.Util.DialogUtil;
import com.weihsuan.welcomecamp.Util.GetBitmap;
import com.weihsuan.welcomecamp.Util.ToastUtil;
import com.weihsuan.welcomecamp.database.Item.Item;
import com.weihsuan.welcomecamp.interfaces.DialogOnclick;

/**
 * A simple {@link Fragment} subclass.
 */
public class UpgradeFragment extends BaseFragment implements DialogOnclick {

    public static int tar;
    public static boolean all = false;
    private int pro[] = {90, 70, 50, 30,0};
    private int down[] = {0, 0, 0, 30,0};
    TextView count, count2;
    int from, to, proNum;


    public UpgradeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_upgrade, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        TextView tv = getActivity().findViewById(R.id.pro);
        if (all) {
            from = 26;
            to = tar;
            proNum = 0;
        } else if (tar<20) {
            from = tar;
            to = tar + 5;
            proNum = tar / 5;
        } else {
            from = tar;
            to = 25;
            proNum = tar/5;
            tv.setVisibility(View.INVISIBLE);
        }
        getActivity().setTitle("升級或分解");
        ImageView arrow = getActivity().findViewById(R.id.arrow);
        arrow.setImageBitmap(GetBitmap.get("arrow", getContext(), 100));
        ImageView left = getActivity().findViewById(R.id.left);
        left.setImageBitmap(GetBitmap.get(from, getContext(), 300));
        ImageView right = getActivity().findViewById(R.id.right);
        right.setImageBitmap(GetBitmap.get(to, getContext(), 300));
        String t = "成功機率：" + pro[proNum] + "\n下降機率：" + down[proNum];
        tv.setText(t);
        count = getActivity().findViewById(R.id.count);
        count.setText(String.valueOf(Item.num[from]));
        count2 = getActivity().findViewById(R.id.count2);
        count2.setText(String.valueOf(Item.num[to]));
        Button bt = getActivity().findViewById(R.id.btn);
        if(from>19&&from<25){
            bt.setVisibility(View.INVISIBLE);
        }
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!pass) {
                    ToastUtil.show("請先登入");
                    return;
                }
                Item.upgrade(from);
                count.setText(String.valueOf(Item.num[from]));
                count2.setText(String.valueOf(Item.num[to]));
            }
        });
        Button del = getActivity().findViewById(R.id.delete);
        del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!pass) {
                    ToastUtil.show("請先登入");
                    return;
                }
                if(Item.num[tar]<=0){
                    ToastUtil.show("數量不足");
                    return;
                }
                DialogUtil.show("分解", "確認分解" + Item.NAME[from], from,
                        getContext(), UpgradeFragment.this);
            }
        });
    }

    @Override
    public void onPositive() {
        Item.delete(tar);
        count.setText(String.valueOf(Item.num[tar]));
        ToastUtil.show("分解獲得"+Item.deleteString[tar/5]);
    }

    @Override
    public void onNegative() {

    }
}
