package com.weihsuan.welcomecamp.fragments;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.weihsuan.welcomecamp.MainActivity;
import com.weihsuan.welcomecamp.R;
import com.weihsuan.welcomecamp.adapter.BagAdapter;
import com.weihsuan.welcomecamp.database.Item.Item;

/**
 * A simple {@link Fragment} subclass.
 */
public class IndexFragment extends BaseFragment {

    Button bag,draw;

    public IndexFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_index, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("首頁");
        bag = getActivity().findViewById(R.id.bag);
        draw = getActivity().findViewById(R.id.draw);
        bag.setOnClickListener(onClickListener);
        draw.setOnClickListener(onClickListener);
        TextView score = getActivity().findViewById(R.id.score);
        score.setText("分數："+Item.getScore());
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(view.getId() == R.id.bag){
                MainActivity.switchFragment(MainActivity.BAG);
            }
            else if(view.getId() == R.id.draw) {
                DrawFragment.type = 0;
                MainActivity.switchFragment(MainActivity.DRAW);
            }
        }
    };
}
