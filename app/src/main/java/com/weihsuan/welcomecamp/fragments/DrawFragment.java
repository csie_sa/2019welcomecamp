package com.weihsuan.welcomecamp.fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;
import com.weihsuan.welcomecamp.MainActivity;
import com.weihsuan.welcomecamp.R;
import com.weihsuan.welcomecamp.Util.DialogUtil;
import com.weihsuan.welcomecamp.Util.ToastUtil;
import com.weihsuan.welcomecamp.database.Item.Item;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * A simple {@link Fragment} subclass.
 */
public class DrawFragment extends BaseFragment {
    private PieChart chart;
    private float pro[] = {60, 20, 15, 5};
    private String name[] = {"初階", "中階", "進階", "最終"};
    private float drawPro[] = {60, 20, 15, 5};
    private String drawName[] = {"初階", "中階", "進階", "最終"};
    private float bookPro[] = {18, 18, 18, 18, 18,10};
    private String bookName[] = {"戰甲", "槍", "盾", "手套", "鞋","全武器"};
    private String color[] = {"#aed6f1", "#d5f5e3", "#fdebd0", "#f5b7b1", "#D2B4DE", "#d5d8dc"};
    private int count = 0;
    private float rotation = 0;
    // draw = 0 , book = 1;
    public static int type;
    private Timer timer;
    View v;
    int t = 1;
    int screenH, screenW;


    public DrawFragment() {
        // Required empty public constructor
    }


    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(type == 0 ? "抽裝" : "抽製作書");
        chartInit();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_draw, container, false);
    }


    private void chartInit() {
        chart = getActivity().findViewById(R.id.chart);
        // Set chart to fill page
//        ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams) chart.getLayoutParams();
//        lp.width = screenH;
//        lp.height = screenH;
//        chart.setLayoutParams(lp);
//        chart.setX(-screenH / 2);
        chart.setUsePercentValues(true);
        chart.getDescription().setEnabled(false);
        chart.setExtraOffsets(5, 5, 5, 5);
        chart.setDragDecelerationFrictionCoef(0.95f);

        chart.setDrawHoleEnabled(true);
        chart.setHoleColor(Color.WHITE);

        chart.setTransparentCircleColor(Color.WHITE);
        chart.setTransparentCircleAlpha(110);

        chart.setHoleRadius(58f);
        chart.setTransparentCircleRadius(61f);

        chart.setDrawCenterText(true);

        chart.setRotationAngle(0);
        Log.i("test", "onCreate: " + chart.getRotationAngle());
        // enable rotation of the chart by touch
        chart.setRotationEnabled(false);
        chart.setHighlightPerTapEnabled(true);


        // chart.setUnit(" €");
        // chart.setDrawUnitsInChart(true);

        // add a selection listener

        Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(true);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);
        l.setEnabled(false);

        Button start = getActivity().findViewById(R.id.start);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!pass) {
                    ToastUtil.show("請先登入");
                    return;
                }
                final float degrees = (float)(Math.random()*360);
                Animation am = new RotateAnimation(rotation,degrees+10800, Animation.RELATIVE_TO_SELF,0.5f, Animation.RELATIVE_TO_SELF,0.5f);
                rotation = degrees;
                am.setDuration(3000);
                am.setRepeatCount(0);
                am.setFillAfter(true);am.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        float a[] = new float[count];
                        for (int i = 0; i < count; i++) {
                            a[i] = pro[i] * 3.6f;
                        }
                        float t = degrees;
                        int i = count - 1;
//                            Log.i("test", "run: " + t);
                        while (t > 0) {
                            if (i < 0) {
                                break;
                            }
                            if (t < a[i]) {
                                break;
                            }
                            t -= a[i];
                            i--;
                        }
                        if (type == 1) {
                            if(i==(count-1)){
                                i = 26;
                            }
                            // book
                            Item.add(i);
                            DialogUtil.show("抽獎結果", "恭喜獲得" + Item.NAME[i], i, getContext());
                        } else if (type == 0) {
                            // draw
                            int r = (int) (Math.random() * 5);
                            int tar = r + i * 5 + 5;
                            Item.add(tar);
                            DialogUtil.show("抽獎結果", "恭喜獲得" + Item.NAME[tar], tar, getContext());
                        }
                        return;
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                chart.startAnimation(am);
            }
        });
        chart.setEntryLabelColor(Color.parseColor("#2c3e50"));
        chart.setEntryLabelTextSize(12f);

        if (type == 0) {
            // draw
            name = drawName;
            pro = drawPro;
            count = 4;
        } else if (type == 1) {
            // book
            name = bookName;
            pro = bookPro;
            count = 6;
        }
        setData();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void setData() {
        ArrayList<PieEntry> entries = new ArrayList<>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.

        for (int i = 0; i < count; i++) {
            entries.add(new PieEntry(pro[i], name[i]));
        }


        PieDataSet dataSet = new PieDataSet(entries, "Election Results");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<>();


        for (int i = 0; i < count; i++) {
            colors.add(Color.parseColor(color[i]));
            Log.i("test", "setData: " + i);
        }

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        dataSet.setDrawValues(false);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter(chart));
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.BLACK);

        chart.setData(data);
        // undo all highlights
        chart.highlightValues(null);
        chart.invalidate();
    }


}
