package com.weihsuan.welcomecamp.fragments;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.weihsuan.welcomecamp.R;
import com.weihsuan.welcomecamp.Util.DialogUtil;
import com.weihsuan.welcomecamp.Util.ToastUtil;
import com.weihsuan.welcomecamp.interfaces.LoginResult;

public class BaseFragment extends Fragment implements LoginResult<String> {

    Menu menu;
    public static boolean pass;
    private static final String PASS = "2019welcome";

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu_login, menu);
        this.menu = menu;
        if (pass) {
            this.menu.getItem(0).setTitle("登出");
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.login) {
            if (pass) {
                pass = false;
                menu.getItem(0).setTitle("登入");
            } else {
                DialogUtil.show("輸入密碼", getContext(), this);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        setHasOptionsMenu(true);
    }

    @Override
    public void onPositive(String obj) {
        if (obj.equals(PASS)) {
            pass = true;
            menu.getItem(0).setTitle("登出");
        } else {
            ToastUtil.show("密碼錯誤");
        }
    }

    @Override
    public void onNegative(String obj) {

    }
}
