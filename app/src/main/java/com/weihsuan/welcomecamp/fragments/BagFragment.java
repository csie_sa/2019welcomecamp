package com.weihsuan.welcomecamp.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.weihsuan.welcomecamp.MainActivity;
import com.weihsuan.welcomecamp.R;
import com.weihsuan.welcomecamp.adapter.BagAdapter;
import com.weihsuan.welcomecamp.interfaces.AllResult;

/**
 * A simple {@link Fragment} subclass.
 */
public class BagFragment extends BaseFragment implements AllResult {
    private static final String TAG = "BagFragment";
    BagAdapter adapter;
    RecyclerView recyclerView;

    public BagFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bag, container, false);
    }


    @Override
    public void onResume() {
        super.onResume();
        setHasOptionsMenu(true);
        getActivity().setTitle("背包");
        recyclerView = getActivity().findViewById(R.id.recycler);
        adapter = new BagAdapter(getContext(), this);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(5, StaggeredGridLayoutManager.VERTICAL));
        recyclerView.addOnItemTouchListener(new RecyclerView.SimpleOnItemTouchListener());
        recyclerView.setAdapter(adapter);
        Button bt = getActivity().findViewById(R.id.book);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DrawFragment.type = 1;
                MainActivity.switchFragment(MainActivity.DRAW);
            }
        });
    }

    @Override
    public void onPositive(int t) {
        UpgradeFragment.all = true;
        UpgradeFragment.tar = t;
        MainActivity.switchFragment(MainActivity.UPGRADE);
    }
}
