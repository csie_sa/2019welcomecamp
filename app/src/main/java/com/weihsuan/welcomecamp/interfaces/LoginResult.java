package com.weihsuan.welcomecamp.interfaces;

public interface LoginResult<E> {
    void onPositive(E obj);
    void onNegative(E obj);
}
