package com.weihsuan.welcomecamp.interfaces;

public interface DialogOnclick {
    void onPositive();
    void onNegative();
}
