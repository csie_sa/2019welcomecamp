package com.weihsuan.welcomecamp.Util;

import android.content.Context;
import android.widget.Toast;

public class ToastUtil {
    private static Toast toast;

    public static void reset(Context context){
        toast = Toast.makeText(context,"",Toast.LENGTH_SHORT);
    }

    public static void show(String msg){
        toast.setText(msg);
        toast.show();
    }
}
