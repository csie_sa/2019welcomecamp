package com.weihsuan.welcomecamp.Util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.weihsuan.welcomecamp.R;
import com.weihsuan.welcomecamp.interfaces.AllResult;
import com.weihsuan.welcomecamp.interfaces.DialogOnclick;
import com.weihsuan.welcomecamp.interfaces.LoginResult;

public class DialogUtil {

    public static void show(String title, String text, int tar, Context c){
        final AlertDialog.Builder builder = new AlertDialog.Builder(c);
        // Get the layout inflater
        LayoutInflater inflater = ((Activity) c).getLayoutInflater();
        final View view = inflater.inflate(R.layout.layout_dialog, null);
        ImageView imageView = view.findViewById(R.id.img);
        imageView.setImageBitmap(GetBitmap.get(tar,c,300));
        TextView tv = view.findViewById(R.id.tv);
        tv.setText(text);
        builder.setView(view);

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        builder.setTitle(title);
        ((Activity) c).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                builder.create().show();
            }
        });
    }
    public static void show(String title, String text,int tar,Context c, final DialogOnclick dialogOnclick){
        final AlertDialog.Builder builder = new AlertDialog.Builder(c);
        // Get the layout inflater
        LayoutInflater inflater = ((Activity) c).getLayoutInflater();
        final View view = inflater.inflate(R.layout.layout_dialog, null);
        ImageView imageView = view.findViewById(R.id.img);
        imageView.setImageBitmap(GetBitmap.get(tar,c,300));
        TextView tv = view.findViewById(R.id.tv);
        tv.setText(text);
        builder.setView(view);

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setPositiveButton("確認", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialogOnclick.onPositive();
            }
        })
        .setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogOnclick.onNegative();
            }
        });
        builder.setTitle(title);
        ((Activity) c).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                builder.create().show();
            }
        });
    }
    public static void show(String title,Context c,final LoginResult<String> loginResult){
        final AlertDialog.Builder builder = new AlertDialog.Builder(c);
        // Get the layout inflater
        LayoutInflater inflater = ((Activity) c).getLayoutInflater();
        final View view = inflater.inflate(R.layout.layout_dialog_login, null);
        builder.setView(view);

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setPositiveButton("確認", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                EditText editText = view.findViewById(R.id.password);
                String t = editText.getText().toString();

                loginResult.onPositive(t);
            }
        })
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        loginResult.onNegative(null);
                    }
                });
        builder.setTitle(title);
        ((Activity) c).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                builder.create().show();
            }
        });
    }
    public static void show(String title, Context c, final AllResult allResult){
        final AlertDialog.Builder builder = new AlertDialog.Builder(c);
        // Get the layout inflater
        LayoutInflater inflater = ((Activity) c).getLayoutInflater();
        final View view = inflater.inflate(R.layout.layout_dialog_all, null);
        builder.setView(view);
        final RadioGroup group = view.findViewById(R.id.group);
        RadioButton rb[] = new RadioButton[5];
        for (int i = 0; i < 5; i++) {
            rb[i] = view.findViewById(c.getResources().getIdentifier("rb"+i,"id",c.getPackageName()));
            rb[i].setBackground(GetBitmap.getDrawable(i+5,c,100));
        }
        rb[0].setSelected(true);
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setPositiveButton("確認", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                RadioButton rb = view.findViewById(group.getCheckedRadioButtonId());
                allResult.onPositive(Integer.valueOf(rb.getTag().toString()));
            }
        })
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
        builder.setTitle(title);
        ((Activity) c).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                builder.create().show();
            }
        });
    }
}
