package com.weihsuan.welcomecamp.Util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.weihsuan.welcomecamp.R;
import com.weihsuan.welcomecamp.fragments.DrawFragment;

public class GetBitmap {
    private static final String TAG = "GetDrawable";
    public static Bitmap get(int tar,Context c,int resize){
//        Log.i(TAG, "get: "+tar);
        Drawable drawable = c.getDrawable(c.getResources().getIdentifier("pic"+tar,"drawable",c.getPackageName()));
        Bitmap b = ((BitmapDrawable)drawable).getBitmap();
        return Bitmap.createScaledBitmap(b, resize, resize, false);
    }

    public static Bitmap get(String name,Context c,int resize){
        Drawable drawable = c.getDrawable(c.getResources().getIdentifier(name,"drawable",c.getPackageName()));
        Bitmap b = ((BitmapDrawable)drawable).getBitmap();
        return Bitmap.createScaledBitmap(b, resize, resize, false);
    }
    public static Drawable getDrawable(int tar,Context c,int resize){
        Drawable rt = null;

        rt = new BitmapDrawable(c.getResources(),get(tar,c,resize));

        return rt;
    }
}
