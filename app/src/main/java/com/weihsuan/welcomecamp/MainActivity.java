package com.weihsuan.welcomecamp;

import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.weihsuan.welcomecamp.database.Item.Item;
import com.weihsuan.welcomecamp.Util.ToastUtil;
import com.weihsuan.welcomecamp.fragments.BagFragment;
import com.weihsuan.welcomecamp.fragments.DrawFragment;
import com.weihsuan.welcomecamp.fragments.IndexFragment;
import com.weihsuan.welcomecamp.fragments.UpgradeFragment;

public class MainActivity extends AppCompatActivity {
    private static int nowPage = 1;
    private static final String TAG = "MainActivity";
    public static int SCREEN_W;
    public static int SCREEN_H;
    private static FragmentManager manager;
    public static final int INDEX = 1;
    public static final int BAG = 2;
    public static final int DRAW = 3;
    public static final int BOOK = 4;
    public static final int UPGRADE = 5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    @Override
    protected void onResume() {
        super.onResume();

        ToastUtil.reset(this);
        Item.init(this);
        manager = getSupportFragmentManager();
        switchFragment(INDEX);
        getScreen();
    }

    private void getScreen(){
        Point point = new Point();
        this.getWindowManager().getDefaultDisplay().getRealSize(point);
        SCREEN_H = point.y;
        SCREEN_W = point.x;
    }

    public static void switchFragment(int page){
        FragmentTransaction trans = manager.beginTransaction();
        switch (page){
            case INDEX:
                trans.replace(R.id.tar,new IndexFragment());
                break;
            case BAG:
                trans.replace(R.id.tar,new BagFragment());
                break;
            case DRAW:
                trans.replace(R.id.tar,new DrawFragment());
                break;
            case BOOK:
                trans.replace(R.id.tar,new IndexFragment());
                break;
            case UPGRADE:
                trans.replace(R.id.tar,new UpgradeFragment());
                break;
        }
        nowPage = page;
        trans.commit();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode==KeyEvent.KEYCODE_BACK){
            switch (nowPage){
                case INDEX:
                    return false;
                case BAG:
                    switchFragment(INDEX);
                    break;
                case DRAW:
                    if(DrawFragment.type == 0){
                        // index
                        switchFragment(INDEX);
                    }
                    else if(DrawFragment.type == 1){
                        switchFragment(BAG);
                    }
                    break;
                case BOOK:
                    switchFragment(BAG);
                    break;
                case UPGRADE:
                    switchFragment(BAG);
                    break;
            }
        }
        return false;
    }
}
