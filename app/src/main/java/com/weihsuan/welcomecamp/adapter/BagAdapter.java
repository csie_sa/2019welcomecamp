package com.weihsuan.welcomecamp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.weihsuan.welcomecamp.MainActivity;
import com.weihsuan.welcomecamp.R;
import com.weihsuan.welcomecamp.Util.DialogUtil;
import com.weihsuan.welcomecamp.Util.GetBitmap;
import com.weihsuan.welcomecamp.Util.ToastUtil;
import com.weihsuan.welcomecamp.database.Item.Item;
import com.weihsuan.welcomecamp.fragments.BagFragment;
import com.weihsuan.welcomecamp.fragments.BaseFragment;
import com.weihsuan.welcomecamp.fragments.UpgradeFragment;
import com.weihsuan.welcomecamp.interfaces.AllResult;

public class BagAdapter extends RecyclerView.Adapter<BagAdapter.ViewHolder> {
    private Context context;
    private BagFragment bagFragment;
    private int num[];
    // TODO set real drawable
    private int res[] = {
            R.drawable.button_ripple,
    };

    public BagAdapter(Context context, BagFragment bagFragment) {
        num = Item.num;
        this.context = context;
        this.bagFragment = bagFragment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup view, int viewType) {
        View v = LayoutInflater.from(view.getContext())
                .inflate(R.layout.adapter_bag, view, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.num.setText(String.valueOf(num[position]));
        // TODO replace with position
        holder.img.setImageBitmap(GetBitmap.get(position, context, 100));
    }

    @Override
    public int getItemCount() {
        return num.length;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView num;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.img);
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) img.getLayoutParams();
            int t = MainActivity.SCREEN_W / 5;
            lp.height = t;
            lp.width = t;
            img.setLayoutParams(lp);
            num = itemView.findViewById(R.id.num);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!BaseFragment.pass) {
                        ToastUtil.show("請先登入");
                        return;
                    }
                    int p = getAdapterPosition();
                    if (Item.num[p] <= 0) {
                        ToastUtil.show("尚未獲得此道具");
                        return;
                    }
                    if (p == 26) {
                        DialogUtil.show("選擇武器", context, (AllResult) bagFragment);
                        return;
                    }
                    if (p == 25) {
                        ToastUtil.show("已到最高階");
                        return;
                    }
                    UpgradeFragment.tar = p;
                    UpgradeFragment.all = false;
                    MainActivity.switchFragment(MainActivity.UPGRADE);
                }
            });
        }
    }


}
